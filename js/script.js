var slider = $(".hero-slider");
slider.owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1,
        dots: false,
        animateOut: 'fadeOut',
    	animateIn: 'fadeIn',
        navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        onInitialized: function() {
        	var a = this.items().length;
            $("#snh-1").html("<span>1</span><span>" + a + "</span>");
        }
    }).on("changed.owl.carousel", function(a) {
        var b = --a.item.index, a = a.item.count;
    	$("#snh-1").html("<span> "+ (1 > b ? b + a : b > a ? b - a : b) + "</span><span>" + a + "</span>");

    });

	slider.append('<div class="slider-nav-warp"><div class="slider-nav"></div></div>');
	$(".hero-slider .owl-nav, .hero-slider .owl-dots").appendTo('.slider-nav');

    // $('.slider').owlCarousel({
	// 	loop: true,
	// 	nav: false,
	// 	dots: false,
	// 	margin : 30,
	// 	autoplay: true,
	// 	navText: ['<i class="flaticon-left-arrow-1"></i>', '<i class="flaticon-right-arrow-1"></i>'],
	// 	responsive : {
	// 		0 : {
	// 			items: 2,
	// 		},
	// 		480 : {
	// 			items: 2,
	// 		},
	// 		768 : {
	// 			items: 3,
	// 		},
	// 		1200 : {
	// 			items: 4,
	// 		}
	// 	}
	// });
        
    var slider = $('.slider');
    slider.owlCarousel({
       items:4,
       loop:true,
       margin:10,
       autoplay:true,
       autoplayTimeout:2000,
       autoplayHoverPause:true
    });
    $('.play').on('click',function(){
        slider.trigger('play.owl.autoplay',[2000])
    })
    $('.stop').on('click',function(){
        slider.trigger('stop.owl.autoplay')
    })

    function openCity(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;
      
        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
      
        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
      
        // Show the current tab, and add an "active" class to the link that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
      }